﻿using Columbia.Data.EntityFramework.Abstractions.Security;
using Columbia.Data.EntityFramework.Core;
using Microsoft.EntityFrameworkCore;
using PlatGO.Portal.Entity;
using PlatGO.Portal.Repository.Abstractions;
using PlatGO.Portal.Repository.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatGO.Portal.Repository
{
    public class IngresoRepository : SystemRepository<Ingreso>, IIngresoRepository
    {
        public IngresoRepository(CoreDbContext dbContext, IUserIdentity userIdentity) : base(dbContext, userIdentity)
        {

        }

        public async Task<IEnumerable<Ingreso>> FindAllIngresos()
        {
            return await FindAll()
                .OrderBy(x => x.Name)
                .ToListAsync();
        }
    }
}
