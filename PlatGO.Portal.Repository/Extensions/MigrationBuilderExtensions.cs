﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PlatGO.Portal.Repository.Extensions
{
    public static class MigrationBuilderExtensions
    {
        public static void ExecSQL(this MigrationBuilder migrationBuilder, string sql)
        {
            migrationBuilder.Sql($@" EXEC (N' {sql.Replace("'", "''")} '); ");
        }
    }
}
