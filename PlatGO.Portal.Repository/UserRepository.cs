﻿using Columbia.Data.EntityFramework.Abstractions.Security;
using Columbia.Data.EntityFramework.Core;
using Microsoft.EntityFrameworkCore;
using PlatGO.Portal.Entity;
using PlatGO.Portal.Repository.Abstractions;
using PlatGO.Portal.Repository.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatGO.Portal.Repository
{
    public class UserRepository : IUserRepository
    {
        public UserRepository(CoreDbContext dbContext) 
        {

        }

        public async Task<IEnumerable<ApplicationUser>> FindAll()
        {
            return await FindAll();
        }
    }
}
