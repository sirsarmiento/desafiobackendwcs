﻿using Microsoft.AspNetCore.Identity;
using PlatGO.Portal.Common;
using PlatGO.Portal.Common.Settings;
using PlatGO.Portal.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace PlatGO.Portal.Repository.Data
{
    public class CoreDbContextSeed
    {
        public static async Task SeedEssentialsAsync(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            //Seed Roles
            await roleManager.CreateAsync(new IdentityRole(Constants.Roles.Administrator.ToString()));
            await roleManager.CreateAsync(new IdentityRole(Constants.Roles.Moderator.ToString()));
            await roleManager.CreateAsync(new IdentityRole(Constants.Roles.User.ToString()));
            //Seed Default User
            var defaultUser = new ApplicationUser { UserName = Authorization.default_username, Email = Authorization.default_email, EmailConfirmed = true, PhoneNumberConfirmed = true };
            if (userManager.Users.All(u => u.Id != defaultUser.Id))
            {
                await userManager.CreateAsync(defaultUser, Authorization.default_password);
                await userManager.AddToRoleAsync(defaultUser, Authorization.default_role.ToString());
            }
        }
    }
}
