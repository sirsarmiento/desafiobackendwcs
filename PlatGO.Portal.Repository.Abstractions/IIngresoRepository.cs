﻿using Columbia.Data.EntityFramework.Abstractions;
using PlatGO.Portal.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlatGO.Portal.Repository.Abstractions
{
    public interface IIngresoRepository : ISystemRepository<Ingreso>
    {
        Task<IEnumerable<Ingreso>> FindAllIngresos();
    }
}
