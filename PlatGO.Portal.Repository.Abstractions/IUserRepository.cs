﻿using Columbia.Data.EntityFramework.Abstractions;
using PlatGO.Portal.Dto.User;
using PlatGO.Portal.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlatGO.Portal.Repository.Abstractions
{
    public interface IUserRepository
    {
        Task<IEnumerable<ApplicationUser>> FindAll();
    }
}
