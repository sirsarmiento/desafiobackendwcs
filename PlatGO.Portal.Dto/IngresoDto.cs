﻿using System;
using static PlatGO.Portal.Common.Constants;

namespace PlatGO.Portal.Dto
{
    public class IngresoDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Identification { get; set; }
        public int Age { get; set; }
        public string House { get; set; }
    }
}
