﻿using Columbia.Api.Dto;
using Columbia.RestClient;
using PlatGO.Portal.Dto.Query;
using PlatGO.Security.RestClient;
using System;
using System.Threading.Tasks;

namespace PlatGo.Portal.Api.Client
{
    public class SiteRestClient : RestClient<SecurityClientOptions>, ISiteRestClient
    {
        public SiteRestClient(SecurityClientOptions options) : base(options)
        {
        }

        public async Task<ApiResponseDto<PublicSiteInfoDto>> GetSite(string queryFilter)
            => await GetAsync<ApiResponseDto<PublicSiteInfoDto>>($"api/public/site/info?queryFilter={queryFilter}");

        public async Task<ApiResponseDto<PublicSiteDto>> GetSiteContent(Guid siteId)
              => await GetAsync<ApiResponseDto<PublicSiteDto>>($"api/public/site/{siteId}");
    }
}
