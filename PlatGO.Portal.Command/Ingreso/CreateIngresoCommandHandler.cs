﻿using AutoMapper;
using Columbia.Data.Transactions.Abstractions;
using Columbia.Domain.Cqrs.Command;
using MediatR;
using PlatGO.Portal.Dto;
using PlatGO.Portal.Repository.Abstractions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PlatGO.Portal.Command.Ingreso
{
    public class CreateIngresoCommandHandler : CommandHandlerBase<CreateIngresoCommand, IngresoDto>
    {
        private readonly IIngresoRepository _ingresoRepository;

        public CreateIngresoCommandHandler(
            CreateUserCommandValidator validator,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IMediator mediator,
            IIngresoRepository academyRepository
        ) : base(unitOfWork, mapper, mediator, validator)
        {
            _ingresoRepository = academyRepository;
        }

        public override async Task<CommandResponse<IngresoDto>> HandleCommand(CreateIngresoCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ingreso = _mapper.Map<Entity.Ingreso>(request.IngresoDto);

                await _ingresoRepository.AddAsync(ingreso);

                var ingresoDto = _mapper.Map<IngresoDto>(ingreso);

                return new CommandResponse<IngresoDto>(ingresoDto, Resources.Ingreso.CreateIngresoSuccess);
            }
            catch (Exception ex)
            {
                throw new Exception(Resources.Ingreso.CreateIngresoError, ex);
            }
        }
    }
}
