﻿using AutoMapper;
using Columbia.Data.Transactions.Abstractions;
using Columbia.Domain.Cqrs.Command;
using MediatR;
using PlatGO.Portal.Dto;
using PlatGO.Portal.Repository.Abstractions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PlatGO.Portal.Command.Ingreso
{
    public class UpdateIngresoCommandHandler : CommandHandlerBase<UpdateIngresoCommand, IngresoDto>
    {
        private readonly IIngresoRepository _ingresoRepository;

        public UpdateIngresoCommandHandler(
            UpdateIngresoCommandValidator validator,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IMediator mediator,
            IIngresoRepository ingresoRepository
        ) : base(unitOfWork, mapper, mediator, validator)
        {
            _ingresoRepository = ingresoRepository;
        }

        public override async Task<CommandResponse<IngresoDto>> HandleCommand(UpdateIngresoCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ingreso = await _ingresoRepository.GetAsync(request.IngresoDto.Id);

                _mapper.Map(request.IngresoDto, ingreso);

                await _ingresoRepository.UpdateAsync(ingreso);

                var ingresoDto = _mapper.Map<IngresoDto>(ingreso);

                return new CommandResponse<IngresoDto>(ingresoDto, Resources.Ingreso.UpdateIngresoSuccess);
            }
            catch (Exception ex)
            {
                throw new Exception(Resources.Ingreso.UpdateIngresoError, ex);
            }
        }
    }
}
