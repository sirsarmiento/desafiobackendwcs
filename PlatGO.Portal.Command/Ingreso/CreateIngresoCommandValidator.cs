﻿using Columbia.Domain.Cqrs.Command;
using FluentValidation;
using PlatGO.Portal.Common;
using PlatGO.Portal.Repository.Abstractions;
using System;
using System.Linq;

namespace PlatGO.Portal.Command.Ingreso
{
    public class CreateUserCommandValidator : CommandValidatorBase<CreateIngresoCommand>
    {
        public CreateUserCommandValidator(IIngresoRepository ingresoRepository)
        {
            RuleFor(x => x.IngresoDto).MustAsync(async (command, ingresoDto, context, cancellationToken) =>
            {
                if (ingresoDto == null)
                    return CustomValidationMessage(context, Resources.Ingreso.Required);

                var ingreso = ingresoDto.Id != default ?
                    await ingresoRepository.GetAsNoTrackingAsync(ingresoDto.Id) : null;

                if (ingreso != null)
                    return CustomValidationMessage(context, Resources.Ingreso.IngresoWithIdAlreadyExists);

                var houseExists = Enum.GetNames(typeof(Constants.House)).Any(x => x.ToLower() == ingresoDto.House.ToLower());

                if (!houseExists)
                    return CustomValidationMessage(context, Resources.Ingreso.HouseNoExists);

                return true;
            }).WithCustomValidationMessage();

            RuleFor(x => x.IngresoDto.Name)
                   .NotEmpty().WithMessage(Resources.Ingreso.NameRequired)
                   .MaximumLength(20).WithMessage(Resources.Ingreso.NameMaxLength);

            RuleFor(x => x.IngresoDto.LastName)
                   .NotEmpty().WithMessage(Resources.Ingreso.LastNameRequired)
                   .MaximumLength(20).WithMessage(Resources.Ingreso.LastNameMaxLength);

            RuleFor(x => x.IngresoDto.Identification)
                   .NotEmpty().WithMessage(Resources.Ingreso.IdentificationRequired);

            RuleFor(x => x.IngresoDto.Age)
                   .NotEmpty().WithMessage(Resources.Ingreso.AgeRequired);
        }
    }
}
