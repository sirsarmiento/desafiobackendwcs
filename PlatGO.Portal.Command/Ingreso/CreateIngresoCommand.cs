﻿using Columbia.Domain.Cqrs.Command;
using PlatGO.Portal.Dto;

namespace PlatGO.Portal.Command.Ingreso
{
    public class CreateIngresoCommand : CommandBase<IngresoDto>
    {
        public IngresoDto IngresoDto { get; set; }

        public CreateIngresoCommand(IngresoDto ingresoDto)
        {
            IngresoDto = ingresoDto;
        }
    }
}
