﻿using Columbia.Domain.Cqrs.Command;
using FluentValidation;
using PlatGO.Portal.Repository.Abstractions;

namespace PlatGO.Portal.Command.Ingreso
{
    public class DeleteIngresoCommandValidator : CommandValidatorBase<DeleteIngresoCommand>
    {
        public DeleteIngresoCommandValidator(IIngresoRepository ingresoRepository)
        {
            RuleFor(x => x.Id).MustAsync(async (command, id, context, cancellationToken) =>
            {
                if (id == default)
                    return CustomValidationMessage(context, Resources.Ingreso.IngresoIdRequired);

                var ingreso = await ingresoRepository.GetAsNoTrackingAsync(id);

                if (ingreso == null)
                    return CustomValidationMessage(context, Resources.Ingreso.IngresoDoesNotExist);

                return true;
            }).WithCustomValidationMessage();
        }
    }
}
