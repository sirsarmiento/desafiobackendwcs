﻿using AutoMapper;
using Columbia.Data.Transactions.Abstractions;
using Columbia.Domain.Cqrs.Command;
using MediatR;
using PlatGO.Portal.Repository.Abstractions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PlatGO.Portal.Command.Ingreso
{
    public class DeleteIngresoCommandHandler : CommandHandlerBase<DeleteIngresoCommand>
    {
        private readonly IIngresoRepository _ingresoRepository;

        public DeleteIngresoCommandHandler(
            DeleteIngresoCommandValidator validator,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IMediator mediator,
            IIngresoRepository academyRepository
        ) : base(unitOfWork, mapper, mediator, validator)
        {
            _ingresoRepository = academyRepository;
        }

        public override async Task<CommandResponse> HandleCommand(DeleteIngresoCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var ingreso = await _ingresoRepository.GetAsync(request.Id);

                await _ingresoRepository.DeleteAsync(ingreso);

                return new CommandResponse(Resources.Ingreso.DeleteIngresoSuccess);
            }
            catch (Exception ex)
            {
                throw new Exception(Resources.Ingreso.DeleteIngresoError, ex);
            }
        }
    }
}
