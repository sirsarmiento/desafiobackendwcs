﻿using Columbia.Domain.Cqrs.Command;
using System;

namespace PlatGO.Portal.Command.Ingreso
{
    public class DeleteIngresoCommand : CommandBase
    {
        public Guid Id { get; set; }

        public DeleteIngresoCommand(Guid id)
        {
            Id = id;
        }
    }
}
