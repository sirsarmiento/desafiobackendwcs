﻿using Columbia.Domain.Cqrs.Command;
using PlatGO.Portal.Dto;

namespace PlatGO.Portal.Command.Ingreso
{
    public class UpdateIngresoCommand : CommandBase<IngresoDto>
    {
        public IngresoDto IngresoDto { get; set; }

        public UpdateIngresoCommand(IngresoDto ingresoDto)
        {
            IngresoDto = ingresoDto;
        }
    }
}
