﻿using AutoMapper;
using Columbia.Api.Dto;
using Columbia.Data.Transactions.Abstractions;
using Columbia.Domain.Cqrs.Command;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using PlatGO.Portal.Api.Settings;
using PlatGO.Portal.Common;
using PlatGO.Portal.Dto;
using PlatGO.Portal.Dto.User;
using PlatGO.Portal.Entity;
using PlatGO.Portal.Repository.Abstractions;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace PlatGO.Portal.Command.User
{
    public class AddRoleCommandHandler : CommandHandlerBase<AddRoleCommand, AddRoleDto>
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public AddRoleCommandHandler(
            AddRoleCommandValidator validator,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IMediator mediator,
            IUserRepository userRepository,
            UserManager<ApplicationUser> userManager, 
            RoleManager<IdentityRole> roleManager, 
            IOptions<JWT> jwt
        ) : base(unitOfWork, mapper, mediator, validator)
        {
            _userManager = userManager;
        }

        public override async Task<CommandResponse<AddRoleDto>> HandleCommand(AddRoleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(request.UserDto.Email);
                if (user == null)
                {
                    return new CommandResponse<AddRoleDto>(request.UserDto, Resources.User.EmailNoExists + request.UserDto.Email);
                    //return $"No Accounts Registered with {request.UserDto.Email}.";
                }

                if (await _userManager.CheckPasswordAsync(user, request.UserDto.Password))
                {
                    var roleExists = Enum.GetNames(typeof(Constants.Roles)).Any(x => x.ToLower() == request.UserDto.Role.ToLower());
                    if (roleExists)
                    {
                        var validRole = Enum.GetValues(typeof(Constants.Roles)).Cast<Constants.Roles>().Where(x => x.ToString().ToLower() == request.UserDto.Role.ToLower()).FirstOrDefault();
                        await _userManager.AddToRoleAsync(user, validRole.ToString());
                        return new CommandResponse<AddRoleDto>(request.UserDto, Resources.User.AddRolSuccess);
                        //return $"Added {request.UserDto.Role} to user {request.UserDto.Email}.";
                    }
                    return new CommandResponse<AddRoleDto>(request.UserDto, Resources.User.RoleNoExists);
                    //return $"Role {request.UserDto.Role} not found.";
                }
                return new CommandResponse<AddRoleDto>(request.UserDto, Resources.User.CredentialsIncorrect);
                //return $"Incorrect Credentials for user {user.Email}.";
            }
            catch (Exception ex)
            {
                throw new Exception(Resources.User.CreateUserError, ex);
            }
        }
    }
}
