﻿using Columbia.Domain.Cqrs.Command;
using FluentValidation;
using PlatGO.Portal.Repository.Abstractions;

namespace PlatGO.Portal.Command.User
{
    public class AddRoleCommandValidator : CommandValidatorBase<AddRoleCommand>
    {
        public AddRoleCommandValidator(IUserRepository userRepository)
        {
            RuleFor(x => x.UserDto).MustAsync(async (command, userDto, context, cancellationToken) =>
            {
                if (userDto == null)
                    return CustomValidationMessage(context, Resources.User.UserRequired);

                if (string.IsNullOrEmpty(userDto.Email))
                    return CustomValidationMessage(context, Resources.User.UserNameRequired);

                return true;
            }).WithCustomValidationMessage();
        }
    }
}
