﻿using AutoMapper;
using Columbia.Data.Transactions.Abstractions;
using Columbia.Domain.Cqrs.Command;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using PlatGO.Portal.Api.Settings;
using PlatGO.Portal.Common.Settings;
using PlatGO.Portal.Dto;
using PlatGO.Portal.Entity;
using PlatGO.Portal.Repository.Abstractions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PlatGO.Portal.Command.User
{
    public class CreateUserCommandHandler : CommandHandlerBase<CreateUserCommand, UserDto>
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public CreateUserCommandHandler(
            CreateUserCommandValidator validator,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IMediator mediator,
            IUserRepository userRepository,
            UserManager<ApplicationUser> userManager, 
            RoleManager<IdentityRole> roleManager, 
            IOptions<JWT> jwt
        ) : base(unitOfWork, mapper, mediator, validator)
        {
            _userManager = userManager;
        }

        public override async Task<CommandResponse<UserDto>> HandleCommand(CreateUserCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var user = new ApplicationUser
                {
                    UserName = request.UserDto.Username,
                    Email = request.UserDto.Email,
                    FirstName = request.UserDto.FirstName,
                    LastName = request.UserDto.LastName
                };
                var userWithSameEmail = await _userManager.FindByEmailAsync(request.UserDto.Email);
                if (userWithSameEmail == null)
                {
                    var result = await _userManager.CreateAsync(user, request.UserDto.Password);
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(user, Authorization.default_role.ToString());
                    }
                    return new CommandResponse<UserDto>(request.UserDto, Resources.User.CreateUserSuccess);
                }
                else
                {
                    return new CommandResponse<UserDto>(request.UserDto, Resources.User.EmailAlreadyExists);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Resources.User.CreateUserError, ex);
            }
        }
    }
}
