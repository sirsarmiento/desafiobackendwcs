﻿using Columbia.Domain.Cqrs.Command;
using PlatGO.Portal.Dto.User;

namespace PlatGO.Portal.Command.User
{
    public class AddRoleCommand : CommandBase<AddRoleDto>
    {
        public AddRoleDto UserDto { get; set; }

        public AddRoleCommand(AddRoleDto userDto)
        {
            UserDto = userDto;
        }
    }
}
