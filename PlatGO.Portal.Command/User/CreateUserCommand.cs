﻿using Columbia.Domain.Cqrs.Command;
using PlatGO.Portal.Dto;

namespace PlatGO.Portal.Command.User
{
    public class CreateUserCommand : CommandBase<UserDto>
    {
        public UserDto UserDto { get; set; }

        public CreateUserCommand(UserDto userDto)
        {
            UserDto = userDto;
        }
    }
}
