using AutoMapper;
using Columbia.Api.Core.Base;
using Columbia.Api.Core.DependencyInjection;
using Columbia.Data.EntityFramework.Abstractions.Security;
using Columbia.Data.Transactions.EntityFramework.DependencyInjection;
using Columbia.Domain.Cqrs.Command.DependencyInjection;
using Columbia.Domain.Cqrs.Query.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using PlatGO.Portal.Api.Security;
using PlatGO.Portal.Api.Settings;
using PlatGO.Portal.Application;
using PlatGO.Portal.Application.Abstractions;
using PlatGO.Portal.Entity;
using PlatGO.Portal.Mapping;
using PlatGO.Portal.Repository.Data;
using System;
using System.Reflection;
using System.Text;
using ColumbiaBootstrap = Columbia.Api.Core.Bootstrap;

namespace PlatGO.Portal.Api
{
    public class Startup : ColumbiaBootstrap.StartupBase
    {
        private readonly IConfiguration _configuration;

        public override ApiOptions ApiOptions => new ApiOptions
        {
            Name = _configuration.GetValue<string>("AppSettings:ApiName"),
            Version = _configuration.GetValue<string>("AppSettings:ApiVersion"),
            Environment = _configuration.GetValue<string>("AppSettings:ApiEnvironment")
        };

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public override void ConfigureBasic(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.ConfigureBasic(app, env);
            app.UseAuthentication();
            app.UseAuthorization();
            // enable Cors
            app.UseCors("MyPolicy");

        }

        public override void ConfigureBasicServices(IServiceCollection services)
        {
            base.ConfigureBasicServices(services);

            //cors
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
        }

        public override void ConfigureDataSources(IServiceCollection services)
        {
            //Configuration from AppSettings
            services.Configure<JWT>(_configuration.GetSection("JWT"));
            //User Manager Service
            services.AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<CoreDbContext>();
            services.AddScoped<IUserApplication, UserApplication>();
            
            //DbContext
            var connectionString = _configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<CoreDbContext>(
                optionsAction => optionsAction.UseSqlServer(connectionString, b => b.MigrationsAssembly("PlatGO.Portal.Repository"))
            );

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                { Title = "JWTapiNetCore", Version = "v1" });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "Enter Bearer [space] and then your valid token in the text input below.\r\n\r\nExample: \"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9\"",
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                    new OpenApiSecurityScheme
                        {
                        Reference = new OpenApiReference
                        {
                             Type = ReferenceType.SecurityScheme,
                             Id = "Bearer"
                        }
                    },
                    new string[] { }
                    }
                });
            });

            //Adding Athentication - JWT
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                      .AddJwtBearer(o =>
                      {
                          o.RequireHttpsMetadata = false;
                          o.SaveToken = false;
                          o.TokenValidationParameters = new TokenValidationParameters
                          {
                              ValidateIssuerSigningKey = true,
                              ValidateIssuer = true,
                              ValidateAudience = true,
                              ValidateLifetime = true,
                              ClockSkew = TimeSpan.Zero,
                              ValidIssuer = _configuration["JWT:Issuer"],
                              ValidAudience = _configuration["JWT:Audience"],
                              IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Key"]))
                          };
                      });

        }

        public override void ConfigureInfrastructureServices(IServiceCollection services)
        {
            //UserIdentity
            services.AddScoped<IUserIdentity, UserIdentity>(sp =>
            {
                return new UserIdentity(sp.GetService<IHttpContextAccessor>()?.HttpContext?.User);
            });

            //Repositories
            services.AddScopedAsInterfaces(Assembly.Load("PlatGO.Portal.Repository"), "Repository");
        }

        public override void ConfigureCrossCutingServices(IServiceCollection services)
        {
            //UnitOfWork
            services.AddEntityFrameworkUnitOfWork<CoreDbContext>();
        }

        public override void ConfigureApplicationServices(IServiceCollection services)
        {
            //AutoMapper
            services.AddAutoMapper(
                typeof(Startup).Assembly,
                typeof(Mappings).Assembly);

            //Application
            services.AddScopedAsInterfaces(Assembly.Load("PlatGO.Portal.Application"), "Application");
        }

        public override void ConfigureDomainServices(IServiceCollection services)
        {
            //Commands
            services.AddCommands("PlatGO.Portal.Command", "Validator");

            //Queries
            services.AddQueries("PlatGO.Portal.Query", "Validator");
        }
    }
}

