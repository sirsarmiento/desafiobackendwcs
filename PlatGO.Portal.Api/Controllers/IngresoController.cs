﻿using Columbia.Api.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PlatGO.Portal.Application.Abstractions;
using PlatGO.Portal.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlatGO.Portal.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class IngresoController : ControllerBase
    {
        private readonly IIngresoApplication _ingresoApplication;

        public IngresoController(IIngresoApplication ingresoApplication)
        {
            _ingresoApplication = ingresoApplication;
        }

        [HttpPost]
        [Route("Create")]
        public async Task<ApiResponseDto<IngresoDto>> Create(IngresoDto ingresoDto)
            => await _ingresoApplication.Create(ingresoDto);

        [HttpPut]
        [Authorize(Roles = "Administrator")]
        [Route("Update")]
        public async Task<ApiResponseDto<IngresoDto>> Update(IngresoDto ingresoDto)
            => await _ingresoApplication.Update(ingresoDto);

        [HttpDelete]
        [Authorize(Roles = "Administrator")]
        [Route("Delete/{id}")]
        public async Task<ApiResponseDto> Delete(Guid id)
            => await _ingresoApplication.Delete(id);

        [HttpGet]
        [Route("Get/{id}")]
        public async Task<ApiResponseDto<IngresoDto>> Get(Guid id)
            => await _ingresoApplication.Get(id);

        [HttpGet]
        [Route("GetAll")]
        public async Task<ApiResponseDto<IEnumerable<IngresoDto>>> GetAll()
            => await _ingresoApplication.GetAll();
    }
}
