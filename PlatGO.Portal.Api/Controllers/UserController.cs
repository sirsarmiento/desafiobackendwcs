﻿using Columbia.Api.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PlatGO.Portal.Application.Abstractions;
using PlatGO.Portal.Dto;
using PlatGO.Portal.Dto.User;
using System.Threading.Tasks;

namespace PlatGO.Portal.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserApplication _userApplication;
        public UserController(IUserApplication userApplication)
        {
            _userApplication = userApplication;
        }

        [HttpPost]
        [Route("create")]
        public async Task<ApiResponseDto<UserDto>> CreateAsync(UserDto model)
           => await _userApplication.Create(model);

        [HttpPost("token")]
        public async Task<ApiResponseDto<AuthenticationDto>> GetTokenAsync(TokenRequestDto model)
           => await _userApplication.GetToken(model.Email, model.Password);

        [HttpPost("add-role")]
        public async Task<ApiResponseDto<AddRoleDto>> AddRole(AddRoleDto model)
          => await _userApplication.AddRole(model);
    }
}
