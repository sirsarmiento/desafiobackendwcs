﻿using Columbia.Data.EntityFramework.Abstractions.Security;
using PlatGO.Portal.Api.Extensions;
using System.Security.Claims;

namespace PlatGO.Portal.Api.Security
{
    public class UserIdentity : IUserIdentity
    {
        readonly ClaimsPrincipal _user;
        public UserIdentity(ClaimsPrincipal user)
        {
            _user = user;
        }

        public string GetUserName() => _user?.GetUserName() ?? "admin";
    }
}
