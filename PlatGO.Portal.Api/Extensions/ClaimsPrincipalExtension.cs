﻿using System;
using System.Security.Claims;

namespace PlatGO.Portal.Api.Extensions
{
    public static class ClaimsPrincipalExtension
    {
        public static string GetId(this ClaimsPrincipal user)
        {
            //TODO: replace with the securoty integration
            //return user.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            return "73E964BB-0499-433D-B0A0-F3CC0DAD1869";
        }

        public static string GetEmail(this ClaimsPrincipal user)
        {
            //TODO: replace with the securoty integration
            //return user.FindFirst("UserEmail")?.Value;
            return "admin@platgo.com";
        }

        public static string GetUserName(this ClaimsPrincipal user)
        {
            //TODO: replace with the securoty integration            
            //return user.FindFirst("UserName")?.Value;
            return "admin";
        }

        public static string GetName(this ClaimsPrincipal user)
        {
            return user.FindFirst(ClaimTypes.Name)?.Value;
        }

        public static bool IsManagementAdministrator(this ClaimsPrincipal user) => true;
    }
}
