﻿using Columbia.Domain.Cqrs.Query;
using PlatGO.Portal.Dto;
using System;

namespace PlatGO.Portal.Query.Ingreso
{
    public class GetIngresoQuery : QueryBase<IngresoDto>
    {
        public Guid Id { get; set; }

        public GetIngresoQuery(Guid id)
        {
            Id = id;
        }
    }
}
