﻿using AutoMapper;
using Columbia.Domain.Cqrs.Query;
using PlatGO.Portal.Dto;
using PlatGO.Portal.Repository.Abstractions;
using System.Threading;
using System.Threading.Tasks;

namespace PlatGO.Portal.Query.Ingreso
{
    public class GetIngresoQueryHandler : QueryHandlerBase<GetIngresoQuery, IngresoDto>
    {
        private readonly IIngresoRepository _ingresoRepository;

        public GetIngresoQueryHandler(IMapper mapper, IIngresoRepository academyRepository) : base(mapper)
        {
            _ingresoRepository = academyRepository;
        }

        protected override async Task<QueryResponse<IngresoDto>> HandleQuery(GetIngresoQuery request, CancellationToken cancellationToken)
        {
            var ingreso = await _ingresoRepository.GetAsNoTrackingAsync(request.Id);

            return new QueryResponse<IngresoDto>(_mapper.Map<IngresoDto>(ingreso));
        }
    }
}
