﻿using Columbia.Domain.Cqrs.Query;
using PlatGO.Portal.Dto;
using System.Collections.Generic;

namespace PlatGO.Portal.Query.Ingreso
{
    public class GetAllIngresoQuery : QueryBase<IEnumerable<IngresoDto>>
    {

    }
}
