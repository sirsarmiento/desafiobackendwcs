﻿using AutoMapper;
using Columbia.Domain.Cqrs.Query;
using PlatGO.Portal.Dto;
using PlatGO.Portal.Repository.Abstractions;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PlatGO.Portal.Query.Ingreso
{
    public class GetAllIngresoQueryHandler : QueryHandlerBase<GetAllIngresoQuery, IEnumerable<IngresoDto>>
    {
        private readonly IIngresoRepository _ingresoRepository;

        public GetAllIngresoQueryHandler(IMapper mapper, IIngresoRepository ingresoRepository) : base(mapper)
        {
            _ingresoRepository = ingresoRepository;
        }

        protected override async Task<QueryResponse<IEnumerable<IngresoDto>>> HandleQuery(GetAllIngresoQuery request, CancellationToken cancellationToken)
        {
            var ingresos = await _ingresoRepository.FindAllIngresos();

            return new QueryResponse<IEnumerable<IngresoDto>>(_mapper.Map<IEnumerable<IngresoDto>>(ingresos));
        }
    }
}
