﻿using AutoMapper;
using Columbia.Domain.Cqrs.Query;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PlatGO.Portal.Api.Settings;
using PlatGO.Portal.Dto;
using PlatGO.Portal.Dto.User;
using PlatGO.Portal.Entity;
using PlatGO.Portal.Repository.Abstractions;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PlatGO.Portal.Query.User
{
    public class GetUserQueryHandler : QueryHandlerBase<GetUserQuery, AuthenticationDto>
    {
        private readonly IUserRepository _userRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly JWT _jwt;

        public GetUserQueryHandler(IMapper mapper, 
            IUserRepository userRepository,
            UserManager<ApplicationUser> userManager,
            IOptions<JWT> jwt
            ) : base(mapper)
        {
            _userRepository = userRepository;
            _userManager = userManager;
            _jwt = jwt.Value;
        }

        protected override async Task<QueryResponse<AuthenticationDto>> HandleQuery(GetUserQuery request, CancellationToken cancellationToken)
        {
            var authenticationModel = new AuthenticationDto();
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null)
            {
                authenticationModel.IsAuthenticated = false;
                authenticationModel.Message = $"No Accounts Registered with {request.Email}.";
                return new QueryResponse<AuthenticationDto>(_mapper.Map<AuthenticationDto>(authenticationModel));
            }
            if (await _userManager.CheckPasswordAsync(user, request.Password))
            {
                authenticationModel.IsAuthenticated = true;
                JwtSecurityToken jwtSecurityToken = await CreateJwtToken(user);
                authenticationModel.Token = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
                authenticationModel.Email = user.Email;
                authenticationModel.UserName = user.UserName;
                var rolesList = await _userManager.GetRolesAsync(user).ConfigureAwait(false);
                authenticationModel.Roles = rolesList.ToList();
                return new QueryResponse<AuthenticationDto>(_mapper.Map<AuthenticationDto>(authenticationModel));
            }
            authenticationModel.IsAuthenticated = false;
            authenticationModel.Message = $"Incorrect Credentials for user {user.Email}.";
            return new QueryResponse<AuthenticationDto>(_mapper.Map<AuthenticationDto>(authenticationModel));
        }
        private async Task<JwtSecurityToken> CreateJwtToken(ApplicationUser user)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            var roles = await _userManager.GetRolesAsync(user);
            var roleClaims = new List<Claim>();
            for (int i = 0; i < roles.Count; i++)
            {
                roleClaims.Add(new Claim("roles", roles[i]));
            }
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim("uid", user.Id)
            }
            .Union(userClaims)
            .Union(roleClaims);
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwt.Key));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            var jwtSecurityToken = new JwtSecurityToken(
                issuer: _jwt.Issuer,
                audience: _jwt.Audience,
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(_jwt.DurationInMinutes),
                signingCredentials: signingCredentials);
            return jwtSecurityToken;
        }
    }
}
