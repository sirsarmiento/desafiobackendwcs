﻿using Columbia.Domain.Cqrs.Query;
using PlatGO.Portal.Dto.User;

namespace PlatGO.Portal.Query.User
{
    public class GetUserQuery : QueryBase<AuthenticationDto>
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public GetUserQuery(string email, string password)
        {
            Email = email;

            Password = password;
        }
    }
}
