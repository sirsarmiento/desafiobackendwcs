﻿using Columbia.Api.Dto;
using PlatGO.Portal.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlatGO.Portal.Application.Abstractions
{
    public interface IIngresoApplication
    {
        Task<ApiResponseDto<IngresoDto>> Create(IngresoDto ingresoDto);
        Task<ApiResponseDto<IngresoDto>> Update(IngresoDto ingresoDto);
        Task<ApiResponseDto> Delete(Guid id);
        Task<ApiResponseDto<IngresoDto>> Get(Guid id);
        Task<ApiResponseDto<IEnumerable<IngresoDto>>> GetAll();
    }
}
