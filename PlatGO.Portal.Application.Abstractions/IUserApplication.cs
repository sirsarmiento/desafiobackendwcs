﻿using Columbia.Api.Dto;
using PlatGO.Portal.Dto;
using PlatGO.Portal.Dto.User;
using System.Threading.Tasks;

namespace PlatGO.Portal.Application.Abstractions
{
    public interface IUserApplication
    {
        Task<ApiResponseDto<UserDto>> Create(UserDto model);
        Task<ApiResponseDto<AuthenticationDto>> GetToken(string email, string password);
        Task<ApiResponseDto<AddRoleDto>> AddRole(AddRoleDto model);
    }
}
