﻿using AutoMapper;
using PlatGO.Portal.Dto;

namespace PlatGO.Portal.Mapping.Ingreso
{
    public class IngresoProfile : Profile
    {
        public IngresoProfile()
        {
            CreateMap<Entity.Ingreso, IngresoDto>()
                .ReverseMap();
        }
    }
}
