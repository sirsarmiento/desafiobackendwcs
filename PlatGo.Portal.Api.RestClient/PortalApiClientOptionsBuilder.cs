﻿using Columbia.RestClient;

namespace PlatGo.Portal.Api.RestClient
{
    public class PortalApiClientOptionsBuilder : RestClientOptionsBuilder<PortalApiClientOptions>
    {
        protected override string RestClientName => "portal-api";

        protected override PortalApiClientOptions BuildOptions(out bool isConfigured)
        {
            isConfigured = true;

            return new PortalApiClientOptions();
        }

        protected override string BuildUrl() => BaseUrl;
    }
}
