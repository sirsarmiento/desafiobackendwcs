﻿using Columbia.Api.Dto;
using Columbia.RestClient;
using PlatGO.Portal.Dto.Query;
using System;
using System.Threading.Tasks;

namespace PlatGo.Portal.Api.RestClient
{
    public class SiteRestClient : RestClient<PortalApiClientOptions>, ISiteRestClient
    {
        public SiteRestClient(PortalApiClientOptions options) : base(options)
        {
        }

        public async Task<ApiResponseDto<PublicSiteInfoDto>> GetSite(string queryFilter)
            => await GetAsync<ApiResponseDto<PublicSiteInfoDto>>($"api/public/site/info?queryFilter={queryFilter}");

        public async Task<ApiResponseDto<PublicSiteDto>> GetSiteContent(Guid siteId)
              => await GetAsync<ApiResponseDto<PublicSiteDto>>($"api/public/site/{siteId}");
    }
}
