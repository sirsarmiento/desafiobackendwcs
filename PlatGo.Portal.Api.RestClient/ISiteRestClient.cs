﻿using Columbia.Api.Dto;
using PlatGO.Portal.Dto.Query;
using System;
using System.Threading.Tasks;

namespace PlatGo.Portal.Api.RestClient
{
    public interface ISiteRestClient
    {
        Task<ApiResponseDto<PublicSiteInfoDto>> GetSite(string queryFilter);

        Task<ApiResponseDto<PublicSiteDto>> GetSiteContent(Guid siteId);
    }
}
