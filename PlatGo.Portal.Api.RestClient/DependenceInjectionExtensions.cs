﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace PlatGo.Portal.Api.RestClient
{
    public static class DependenceInjectionExtensions
    {
        public static IServiceCollection UsePortalApiClient(this IServiceCollection services, Action<PortalApiClientOptionsBuilder> optionsBuilder)
        {
            var _optionsBuilder = new PortalApiClientOptionsBuilder();

            optionsBuilder.Invoke(_optionsBuilder);

            var options = _optionsBuilder.Build();

            if (options == null)
                throw new ArgumentNullException("options");

            //Options
            services.AddSingleton(options);
            //Services
            services.AddSingleton<ISiteRestClient, SiteRestClient>();

            return services;
        }
    }
}
