﻿using System.ComponentModel.DataAnnotations;

namespace PlatGO.Portal.Common
{
    public static class Constants
    {
        public enum Roles
        {
            Administrator,
            Moderator,
            User
        }

        public enum House
        {
            Gryffindor,
            Hufflepuff,
            Ravenclaw,
            Slytherin
        }
    }
}
