﻿using System;
using System.Linq;
using System.Reflection;

namespace PlatGO.Portal.Common.Extension
{
    public static class EnumExtenstion
    {
        public static TAttribute GetAttribute<TAttribute>(this Enum enumValue)
                where TAttribute : Attribute
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<TAttribute>();
        }
    }
}
