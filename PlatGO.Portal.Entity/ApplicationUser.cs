﻿using Microsoft.AspNetCore.Identity;

namespace PlatGO.Portal.Entity
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
