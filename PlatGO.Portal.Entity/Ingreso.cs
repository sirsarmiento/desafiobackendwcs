﻿using Columbia.Data.Core.System;
using System;
using System.ComponentModel.DataAnnotations;

namespace PlatGO.Portal.Entity
{
    public class Ingreso : SystemEntity
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        [Required]
        [MaxLength(20)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(10)]
        public int Identification { get; set; }


        [Required]
        [MaxLength(2)]
        [Range(1, 99)]
        public short Age { get; set; }

        [Required]
        public string House { get; set; }
    }
}
