﻿
using Columbia.Api.Dto;
using Columbia.Application.Core;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using PlatGO.Portal.Api.Settings;
using PlatGO.Portal.Application.Abstractions;
using PlatGO.Portal.Command.User;
using PlatGO.Portal.Dto;
using PlatGO.Portal.Dto.User;
using PlatGO.Portal.Entity;
using PlatGO.Portal.Query.User;
using System.Threading.Tasks;

namespace PlatGO.Portal.Application
{
    public class UserApplication : ApplicationBase, IUserApplication
    {
     
        public UserApplication(IMediator mediator) : base(mediator)
        {
       
        }

        public async Task<ApiResponseDto<UserDto>> Create(UserDto model)
            => await HandleAndCastCommand<CreateUserCommand, UserDto>(new CreateUserCommand(model));
        public async Task<ApiResponseDto<AuthenticationDto>> GetToken(string email, string password)
            => await HandleAndCastQuery<GetUserQuery, AuthenticationDto>(new GetUserQuery(email, password));
        public async Task<ApiResponseDto<AddRoleDto>> AddRole(AddRoleDto model)
        => await HandleAndCastCommand<AddRoleCommand, AddRoleDto>(new AddRoleCommand(model));
    }
}
