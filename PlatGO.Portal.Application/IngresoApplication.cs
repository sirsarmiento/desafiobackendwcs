﻿using Columbia.Api.Dto;
using Columbia.Application.Core;
using MediatR;
using PlatGO.Portal.Application.Abstractions;
using PlatGO.Portal.Command.Ingreso;
using PlatGO.Portal.Dto;
using PlatGO.Portal.Query.Ingreso;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlatGO.Portal.Application
{
    public class IngresoApplication : ApplicationBase, IIngresoApplication
    {
        public IngresoApplication(IMediator mediator) : base(mediator)
        {

        }

        public async Task<ApiResponseDto<IngresoDto>> Create(IngresoDto ingresoDto)
            => await HandleAndCastCommand<CreateIngresoCommand, IngresoDto>(new CreateIngresoCommand(ingresoDto));

        public async Task<ApiResponseDto<IngresoDto>> Update(IngresoDto ingresoDto)
            => await HandleAndCastCommand<UpdateIngresoCommand, IngresoDto>(new UpdateIngresoCommand(ingresoDto));

        public async Task<ApiResponseDto> Delete(Guid id)
            => await HandleAndCastCommand(new DeleteIngresoCommand(id));

        public async Task<ApiResponseDto<IngresoDto>> Get(Guid id)
            => await HandleAndCastQuery<GetIngresoQuery, IngresoDto>(new GetIngresoQuery(id));

        public async Task<ApiResponseDto<IEnumerable<IngresoDto>>> GetAll()
            => await HandleAndCastQuery<GetAllIngresoQuery, IEnumerable<IngresoDto>>(new GetAllIngresoQuery());
    }
}
